# pysemigroup

A python implementation of semigroup algorithms. 
Algorithms used here are mostly based on the lectures notes of [Jean-Éric Pin](https://www.irif.fr/~jep//PDF/MPRI/MPRI.pdf)
Graphics output depends of graphviz being installed. 

Not tested on non linux systems. 

Installation procedure through pip: 

``` 
pip3 install pysemigroup
```

# Usage 
Notebook will come hopefully soon. 

# Online tools

An easy to use online interface can be found [here](https://paperman.name/semigroup/)